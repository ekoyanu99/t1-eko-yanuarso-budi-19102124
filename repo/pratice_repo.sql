-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jan 2022 pada 07.57
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pratice_repo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `nama`, `kategori`, `website`, `twitter`, `telegram`, `image`, `deskripsi`, `note`, `created_at`, `updated_at`) VALUES
(1, 'PuddingSwap', 'Project', 'https://puddingswap.finance/', 'https://twitter.com/PuddingSwap', 't.me/Dex_PuddingSwap', 'assets/images\\user-1643105247.png', 'Pudding Finance is a decentralized exchange running on Hoo Smart Chain, with lots of features that let you earn and win tokens. It is fast, cheap and easily accessible as it is not difficult to use.\r\n\r\nPudingSwap helps you make the most out of your crypto in three ways : Trade, Earn, and Win.\r\nActivate avatar you will be able to join IDO on Pudding Finance.\r\n\r\nFuture developmet :\r\n\r\nPartner pools\r\nIDO\r\nAuto-compounding\r\nOther products :\r\n\r\nLottery\r\nAnalytics\r\nVoting', 'Pudding Finance is a decentralized exchange running on Hoo Smart Chain, with lots of features that let you earn and win tokens. It is fast, cheap and easily accessible as it is not difficult to use.', '2022-01-25 03:07:28', '2022-01-25 03:08:15'),
(2, 'Same Finance', 'Project', 'https://www.same.finance/', 'https://twitter.com/SameFinance', 'https://t.me/Samecoin', 'assets/images\\user-1643105387.png', 'Same Finance is leveraged yield farming with focus on stable income and growth. SameFinance is available on the HECO and HSC blockchain, with later release on BSC and POLYGON in Q1 2022 onwards. Same Finance cleverly links both borrowers and lenders through a series of complex contract mechanisms to continuously improve the overall capital utilization rate of the DeFi market. While bringing a higher rate of return, it also maximizes the safety of user funds.', 'Same Finance is leveraged yield farming with focus on stable income and growth. SameFinance is available on the HECO and HSC blockchain, with later release on BSC and POLYGON in Q1 2022 onwards.', '2022-01-25 03:09:47', '2022-01-25 03:09:47'),
(3, 'Crypto Card', 'Project', 'https://www.cryptioncard.com/', 'https://twitter.com/Crpto_Card', 'https://t.me/Crypto_Card1', 'assets/images\\user-1643105478.png', 'Crypto Card is a protocol offering an infrastructure that combines GameFi, NFT, and DeFi. It can attract users to join the blockchain space as an independent and open NFT ecosystem. Through the Crypto Card NFT kit, anyone can issue his NFTs to join a game. Also, the issued NFTs can be traded or auctioned. In this way, the whole life cycle of an NFT can be covered.', 'Crypto Card is a protocol offering an infrastructure that combines GameFi, NFT, and DeFi. It can attract users to join the blockchain space as an independent and open NFT ecosystem.', '2022-01-25 03:11:18', '2022-01-25 03:11:18'),
(4, 'Slowmist', 'Partner', 'https://www.slowmist.com/', 'https://twitter.com/SlowMist_Team', NULL, 'assets/images\\user-1643105578.png', 'SlowMist is a company focused on blockchain ecological security. It was founded in January 2018. It was founded by a team with more than ten years of front-line network security attack-defense experiences, and the team members have created the security project with world-class influence. SlowMist Technology is already a top international blockchain security company, served many global well-known projects mainly through \"the security solution that integrated the threat discovery and threat defense while tailored to local conditions\", there are thousands of commercial customers, customers distributed in more than a dozen major countries and regions.', 'SlowMist is a company focused on blockchain ecological security. It was founded in January 2018. It was founded by a team with more than ten years of front-line network security attack-defense experiences, and the team members have created the security project with world-class influence.', '2022-01-25 03:12:58', '2022-01-25 03:12:58'),
(5, 'Certik', 'Partner', 'https://www.certik.io/', 'https://twitter.com/certik_io', NULL, 'assets/images\\user-1643105662.png', 'Founded in 2018 by professors of Yale University and Columbia University, CertiK is a pioneer in blockchain security, utilizing best-in-class AI technology to secure and monitor blockchain protocols and smart contracts. CertiK’s mission is to secure the cyber world. Starting with blockchain, CertiK applies cutting-edge innovations from academia into enterprise, enabling mission-critical applications to be built with security and correctness.', 'Founded in 2018 by professors of Yale University and Columbia University, CertiK is a pioneer in blockchain security, utilizing best-in-class AI technology to secure.', '2022-01-25 03:14:22', '2022-01-25 03:14:22'),
(6, 'Token Pocket', 'Wallet', 'https://www.tokenpocket.pro/', 'https://twitter.com/TokenPocket_TP', 'https://t.me/tokenpocket_en', 'assets/images\\user-1643105744.png', 'TokenPocket is a world-leading digital currency wallet, supporting public blockchains including BTC, ETH, BSC, HECO, TRON, OKExChain, Polkadot, Kusama, EOS and Layer 2. It provides reliable digital currency asset management services for nearly 10 million users from more than 200 countries and regions.', 'TokenPocket is a world-leading digital currency wallet, supporting public blockchains including BTC, ETH, BSC, HECO, TRON, OKExChain, Polkadot.', '2022-01-25 03:15:44', '2022-01-25 03:15:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_logins`
--

CREATE TABLE `admin_logins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admin_logins`
--

INSERT INTO `admin_logins` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, '19102124', 'kelasti1', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2022_01_19_200810_create_admins_table', 1),
(4, '2022_01_20_051814_create_admin_logins_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `admin_logins`
--
ALTER TABLE `admin_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `admin_logins`
--
ALTER TABLE `admin_logins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
