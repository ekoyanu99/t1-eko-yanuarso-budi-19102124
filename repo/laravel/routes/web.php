<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/admin/create','AdminController@create')->name('admin.create')->middleware('login_auth');
Route::post('/admin','AdminController@store')->name('admin.store')->middleware('login_auth');
Route::get('/admin','AdminController@index')->name('admin.index')->middleware('login_auth');
Route::get('/admin/{admin}','AdminController@show')->name('admin.show')->middleware('login_auth');
Route::get('/admin/{admin}/edit', 'AdminController@edit')->name('admin.edit')->middleware('login_auth');
Route::patch('/admin/{admin}', 'AdminController@update')->name('admin.update')->middleware('login_auth');
Route::delete('/admin/{admin}', 'AdminController@destroy')->name('admin.destroy')->middleware('login_auth');

Route::get('/register', 'AdminLoginController@register')->name('login.register')->middleware('login_auth');
Route::post('/register/store','AdminLoginController@store')->name('login.store')->middleware('login_auth');

Route::get('/login', 'AdminLoginController@index')->name('login.index');
Route::get('/logout', 'AdminLoginController@logout')->name('login.logout');
Route::post('/login', 'AdminLoginController@process')->name('login.process');

Route::get('/','PostController@index')->name('user.index');
Route::get('/{user}','PostController@show')->name('user.show');
