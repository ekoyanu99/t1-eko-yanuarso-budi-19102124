<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
    <style>
        /* Gak terlalu penting intinya background buat area slider */
        .testimonial-area {
            padding: 100px 0 0;
            position: relative;
            background-color: linear-gradient(#69fadb, #00aee9);
        }

        .testimonial-area::before {
            position: absolute;
            content: "";
            /* background: rgba(0, 0, 0, 9); */
            width: 50%;
            height: 50%;
        }

        .single-testimonial {
            color: #fff;
            padding-left: 75px;
            position: relative;
            margin: 0 15px 15px;
        }

        .img-area {
            border-radius: 50%;
            height: 150px;
            left: 0;
            margin-top: -75px;
            position: absolute;
            top: 50%;
            width: 150px;
        }

        .testi-text {
            background: #91ffff;
            padding: 40px 20px 40px 100px;
            color: #2626;
        }

        .testi-text h4 {
            font-size: 30px;
            margin-bottom: 30px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .testi-text h4 span {
            display: block;
            font-size: 14px;
        }

        .testi-text p {
            font-family: "Times New Roman", Times, serif;
            line-height: 25px;
            font-size: 18px;
        }

        .single-testimonial:hover .testi-text {
            background: #fff;
            transition: 0.9s;
        }

        .testimonial-area div.owl-nav {
            color: #ffff4a;
            display: inline-block;
            font-size: 55px;
            height: 60px;
            margin-top: -30px;
            text-align: center;
            width: 40px;
            transition: 0.3s;
        }

        .owl-prev,
        .owl-next {
            position: absolute;
        }

        .owl-prev {
            left: -20px;
            top: 100px;
        }

        .owl-next {
            right: -20px;
            top: 100px;
        }

        /* Ini biar resposif */
        /* Responsive css */
        @media only screen and (max-width: 767px) {
            .testimonial-area {
                padding: 15px 0;
            }

            .single-testimonial {
                padding-left: 0;
                padding-top: 75px;
            }

            .img-area {
                left: 50%;
                margin-left: -75px;
                top: 75px;
            }

            .testi-text {
                padding: 80px 25px 25px;
            }

            .testi-text h4 {
                font-size: 20px;
                margin-bottom: 20px;
            }

            .testi-text p {
                font-size: 16px;
            }

            .owl-prev {
                left: -15px;
                top: 150px;
            }

            .owl-next {
                right: -15px;
                top: 150px;
            }
        }

        body,
        html {
            height: 100%;
            line-height: 1.8;
            font-family: "Arvo", serif;
            overflow-x: hidden;
            background-image: linear-gradient(#69fadb, #00aee9);
            text-align: center;
            color: rgb(27, 41, 94);
        }

        .gambar {
            height: 24px;
            width: 28px;
            position: center;
        }

        .logo {
            height: 30px;
            width: 54px;
        }

        .w3-top {
            height: 72px;
            position: fixed;
            z-index: 3;
        }

        .w3-bar .w3-button {
            padding: 16px;
        }

        button {
            margin: 20px;
        }

        .kategori {
            width: 90%;
            margin: 40px auto;
            text-align: center;
            box-sizing: inherit;
            display: flex;
            justify-content: center;
            flex-wrap: nowrap;
            padding: 8px;
        }

        /*button kategori*/
        #kategori-btn {
            width: 130px;
            height: 40px;
            color: #fff;
            border-radius: 5px;
            padding: 10px 25px;
            font-family: "Lato", sans-serif;
            font-weight: 500;
            kground: transparent;
            cursor: pointer;
            transition: all 0.3s ease;
            position: relative;
            box-shadow: inset 2px 2px 2px 0px rgba(255, 255, 255, 0.5),
                7px 7px 20px 0px rgba(0, 0, 0, 0.1), 4px 4px 5px 0px rgba(0, 0, 0, 0.1);
        }

        /* 13 */
        .btn-13 {
            background-color: #89d8d3;
            background-image: linear-gradient(315deg, #89d8d3 0%, #03c8a8 74%);
            border: none;
            margin: 8px 8px 8px 8px;
        }

        .btn-13:after {
            position: absolute;
            content: "";
            width: 100%;
            height: 0;
            bottom: 0;
            left: 0;
            z-index: -1;
            border-radius: 5px;
            background-color: #4dccc6;
            background-image: linear-gradient(315deg, #4dccc6 0%, #96e4df 74%);
            box-shadow: -7px -7px 20px 0px #fff9, -4px -4px 5px 0px #fff9,
                7px 7px 20px 0px #0002, 4px 4px 5px 0px #0001;
            transition: all 0.3s ease;
        }

        .btn-13:hover {
            color: #fff;
        }

        .btn-13:hover:after {
            top: 0;
            height: 100%;
        }

        .btn-13:active {
            top: 2px;
        }

        .card {
            box-shadow: -3px -3px aqua;
            margin: 1rem;
            text-align: justify;
            background: white;
            border-radius: 2px;
            max-width: 300px;
            position: relative;
            display: inline-block;
            padding-top: 5px;
            padding-right: 5px;
            padding-left: 5px;
            padding-bottom: 5px;
            transition: box-shadow 0.2s;
        }

        a {
            text-decoration: none;
            font-size: 15px;
            color: black;
        }

        .card:hover {
            box-shadow: 0 0 80px -10px aquamarine;
        }

        button:hover,
        a:hover {
            opacity: 0.7;
        }

        .sticky {
            position: fixed;
            top: 0;
            width: 100%;
        }

    </style>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway"> -->
    <link href="https://fonts.googleapis.com/css2?family=Arvo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- moralis -->
    <script src="https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"></script>
    <script src="https://unpkg.com/moralis/dist/moralis.js"></script>

    <title>Hooniverse</title>
</head>

<body>
    <div class="header">
        <div class="w3-top">
            <div class="w3-bar w3-white w3-card" id="myNavbar">
                <a href="#home" class="w3-bar-item w3-button">
                    <img class="logo" src="https://www.hoosmartchain.com/images/line3-1.a41dcbec.png">
                </a>

                <!-- Right-sided navbar links -->
                <div class="w3-right ">
                    <!-- Connect Wallet -->
                    <a href="#connect" class="w3-bar-item w3-button" id="btn-login">Connect</a>
                    <a href="#connect" class="w3-bar-item w3-button" id="btn-logout">Logout</a>
                </div>

            </div>
        </div>
    </div>



    <div class="testimonial-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel test-slides" id="testimonial-carousel">

                        @forelse ($users as $post)
                            <div class="item"> {{ $loop->parent }}
                                <div class="single-testimonial">
                                    <div class="img-area">
                                        <img src="{{ url('') }}/{{ $post->image }}" class="rounded"
                                            alt="">
                                    </div>
                                    <div class="testi-text">
                                        <h4>{{ $post->nama }} <span>{{ $post->kategori }}</span></h4>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <td colspan="6" class="text-center">Tidak ada data...</td>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kategori">
        <button id="kategori-btn" class="btn-13">Project</button>
        <button id="kategori-btn" class="btn-13">Partner</button>
        <button id="kategori-btn" class="btn-13">Exchange</button>
        <button id="kategori-btn" class="btn-13">Wallet</button>
    </div>

    @forelse ($users as $post)
        <div class="card">{{ $loop->parent }}
            <img src="{{ url('') }}/{{ $post->image }}" class="rounded" alt="" style="width:50%"
                align="center">
            <br>
            <h1>{{ $post->nama }}</h1>
            <p>{{ $post->note }}</p>
            <a href="{{ $post->website }}"><i class="fa fa-dribbble"></i></a>
            <a href="{{ $post->twitter }}"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <p><button><a href="{{ route('user.show', ['user' => $post->id]) }}">Learn More</a></button></p>
        </div>
    @empty
        <td colspan="6" class="text-center">Tidak ada data...</td>
    @endforelse

    <!-- Footer -->
    <footer class="w3-center w3-white w3-padding-64">
        <button onclick="topFunction()" id="scroll-btn" title="Top" class="w3-button w3-light-grey"><i
                class="fa fa-arrow-up w3-margin-right"></i>To the top</button>
        <div class="w3-xlarge w3-section">
            <i class="fa fa-dribbble w3-hover-opacity"></i>
            <i class="fa fa-github w3-hover-opacity"></i>
            <i class="fa fa-gitlab w3-hover-opacity"></i>
            <i class="fa fa-linkedin w3-hover-opacity"></i>
        </div>
        <h6>Powered by <a href="#" target="_blank" class="w3-hover-text-green">Repo Teams</a></h6>
        </div>
    </footer>


    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/jquery/dist/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script>
        // config awal owl
        jQuery(document).ready(function($) {
            $('.owl-carousel').owlCarousel({
                margin: 10,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            })
        })

        $('#testimonial-carousel').owlCarousel({
            autoplay: true,
            dots: false,
            nav: true,
            loop: true,
            navText: ['<i class = "fa fa-angle-left"></i>', '<i class = "fa fa-angle-right"></i>'],
            // margin: 10,
            // nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 2
                }
            }
        })
        // akhir owl

        // connect to Moralis server
        const serverUrl = "https://egjfmsxacnal.usemoralis.com:2053/server";
        const appId = "aWHYPBPM0KnW2GelYo97GUZHq7LTH7cQRAWoXg0J";
        Moralis.start({
            serverUrl,
            appId
        });

        // LOG IN WITH METAMASK
        async function login() {
            let user = Moralis.User.current();
            if (!user) {
                user = await Moralis.authenticate();
                $("#btn-login").hide();
                $("#btn-logout").show();
            }
            console.log("logged in user:", user);
            getStats();
        }

        // LOG OUT
        async function logOut() {
            await Moralis.User.logOut();
            console.log("logged out");
            $("#btn-login").show();
            $("#btn-logout").hide();
        }

        $(document).ready(function() {

        });

        // bind button click handlers
        document.getElementById("btn-login").onclick = login;
        document.getElementById("btn-logout").onclick = logOut;
        document.getElementById("btn-get-stats").onclick = getStats;

        // refresh stats
        function getStats() {
            const user = Moralis.User.current();
            if (user) {
                getUserTransactions(user);
            }
            getAverageGasPrices();
        }

        // HISTORICAL TRANSACTIONS
        async function getUserTransactions(user) {
            // create query
            const query = new Moralis.Query("EthTransactions");
            query.equalTo("from_address", user.get("ethAddress"));

            // subscribe to query updates
            const subscription = await query.subscribe();
            handleNewTransaction(subscription);

            // run query
            const results = await query.find();
            console.log("user transactions:", results);
        }

        // REAL-TIME TRANSACTIONS
        async function handleNewTransaction(subscription) {
            // log each new transaction
            subscription.on("create", function(data) {
                console.log("new transaction: ", data);
            });
        }

        // CLOUD FUNCTION
        async function getAverageGasPrices() {
            const results = await Moralis.Cloud.run("getAvgGas");
            console.log("average user gas prices:", results);
            renderGasStats(results);
        }

        function renderGasStats(data) {
            const container = document.getElementById("gas-stats");
            container.innerHTML = data
                .map(function(row, rank) {
                    return `<li>#${rank + 1}: ${Math.round(row.avgGas)} gwei</li>`;
                })
                .join("");
        }

        //get stats on page load
        getStats();


        // top function
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>
