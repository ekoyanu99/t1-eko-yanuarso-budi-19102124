<!DOCTYPE html>
<html>

<head>
    <title>Detail {{ $user->nama }}</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway"> -->
    <link href="https://fonts.googleapis.com/css2?family=Arvo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        * {
            box-sizing: border-box;
        }

        /* Style the body */
        body {
            font-family: "Arvo", serif;
            margin: 0;
            color: rgb(27, 41, 94);
        }

        /* Header/logo Title */
        .header {
            height: 72px;
            /* padding: 0 16px 0 16px; */
            display: flex;
            background-color: #ffff;
        }

        .logo {
            height: 30px;
            width: 54px;
        }

        /* Style the top navigation bar */
        .navbar {
            display: flex;
        }

        /* Style the navigation bar links */
        .navbar a {
            color: rgb(27, 41, 94);
            padding: 14px 20px;
            text-decoration: none;
            text-align: center;
        }

        /* Change color on hover */
        .navbar a:hover {
            background-color: #ddd;
            color: black;
        }

        /* Column container */
        .row {
            display: flex;
            flex-wrap: wrap;
        }

        /* Create two unequal columns that sits next to each other */
        /* Sidebar/left column */
        .side {
            flex: 30%;
            background-color: white;
            padding: 20px;
        }

        /* Main column */
        .main {
            flex: 70%;
            background-color: white;
            padding: 20px;
        }

        /* image */
        .side img {
            background-color: white;
            width: 100%;
            height: auto;
            padding: 20px;
        }

        .sosmed ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .sosmed a {
            text-decoration: none;
        }

        .sosmed a:hover {
            color: black;
        }

        .row button {
            background-color: #00c0ab;
        }

        .main button {
            width: 100%;
            height: auto;
            background-color: #00c0ab;
        }

        p {
            text-align: justify;
        }

        /* Footer */
        .footer {
            padding: 20px;
            text-align: center;
            background: #ddd;
        }

        /* Responsive layout - when the screen is less than 700px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 700px) {
            .row {
                flex-direction: column;
            }
        }

    </style>
</head>

<body>
    <!-- Header -->
    <div class="header">
        <div class="w3-top">
            <div class="w3-bar w3-white w3-card" id="myNavbar">
                <a href="#home" class="w3-bar-item w3-button">
                    <img class="logo" src="https://www.hoosmartchain.com/images/line3-1.a41dcbec.png">
            </div>
        </div>
    </div>

    <!-- Navigation Bar -->
    <div class="navbar">
        <a href="{{ route('user.index') }}">Home</a>
        <a href="#">{{ $user->kategori }}</a>
        <a href="#">{{ $user->nama }}</a>
    </div>

    <!-- The flexible grid (content) -->
    <div class="row">
        <div class="side">
            <div class="img">
                <img src="{{ $user->image }}" alt="" srcset="" />
            </div>

            <h2>{{ $user->nama }}</h2>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-o"></i>
            <p>4.3 from 9 reviews</p>

            <div class="sosmed">
                <ul>
                    <li>
                        <a href="{{ $user->website }}">
                            <i class="fa fa-internet-explorer"></i>
                            Website</a>
                    </li>
                    <li>
                        <a href="{{ $user->twitter }}">
                            <i class="fa fa-twitter"></i>
                            Twitter</a>
                    </li>
                    <li>
                        <a href="{{ $user->telegram }}">
                            <i class="fa fa-telegram"></i>
                            Telegram</a>
                    </li>
                </ul>
            </div>

            <h4>Tags</h4>
            <!-- Nyoba -->
            <button>{{ $user->kategori }}</button>
            {{-- <button></button>
            <button></button> --}}
        </div>
        <div class="main">
            <!--Isinya-->
            <p>
                {{ $user->deskripsi }}
            </p>
            <hr />
            <!--Akhir isi sebelum review-->
            <br />
            <h2>Reviews</h2>
            <button><i class="fa fa-star-o"></i> Write a Review </button>
            <h3>Sort by</h3>
            <input type="radio" />
            <label for="sortby">Newest First</label>

            <!--Isinya review-->
            <div class="card"></div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="w3-center w3-white w3-padding-64">
        <button onclick="topFunction()" id="scroll-btn" title="Top" class="w3-button w3-light-grey"><i
                class="fa fa-arrow-up w3-margin-right"></i>To the top</button>
        <div class="w3-xlarge w3-section">
            <i class="fa fa-dribbble w3-hover-opacity"></i>
            <i class="fa fa-github w3-hover-opacity"></i>
            <i class="fa fa-gitlab w3-hover-opacity"></i>
            <!--
        <i class="fa fa-pinterest-p w3-hover-opacity"></i>
        <i class="fa fa-twitter w3-hover-opacity"></i> -->
            <i class="fa fa-linkedin w3-hover-opacity"></i>
        </div>
        <h6>Powered by <a href="#" target="_blank" class="w3-hover-text-green">Repo Teams</a></h6>
        </div>
    </footer>

    <script>
        // top function
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>
