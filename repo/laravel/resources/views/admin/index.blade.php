@extends('admin_layout.app')
@section('header')
    @include('admin_layout.header')
@endsection
@section('leftbar')
    @include('admin_layout.leftbar')
@endsection
@section('rightbar')
    @include('admin_layout.rightbar')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hoosmartchain
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container mt-3">
                <div class="row">
                    <div class="col-12">
                        <div class="py-4 d-flex justify-content-end align-items-center">
                            <h3>Tabel Konten</h3>
                            <a href="{{ route('admin.create') }}" class="btn btn-primary">Tambah Konten</a>
                        </div>
                        @if (session()->has('pesan'))
                            <div class="alert alert-success">
                                {{ session()->get('pesan') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Foto</th>
                                    <th>Nama</th>
                                    <th>Kategori</th>
                                    <th>Website</th>
                                    <th>Twitter</th>
                                    <th>Telegram</th>
                                    <th>Deskripsi</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($admins as $konten)
                                    <tr>
                                        <th>{{ $loop->iteration }}</th>
                                        <td><img height="30px" src="{{ url('') }}/{{ $konten->image }}"
                                                class="rounded" alt=""></td>
                                        <td>
                                            <a
                                                href="{{ route('admin.show', ['admin' => $konten->id]) }}">{{ $konten->nama }}</a>
                                        </td>
                                        <td>{{ $konten->kategori }}</td>
                                        <td>{{ $konten->website }}</td>
                                        <td>{{ $konten->twitter }}</td>
                                        <td>{{ $konten->telegram }}</td>
                                        <td>{{ $konten->deskripsi }}</td>
                                        <td>{{ $konten->note }}</td>
                                    </tr>
                                @empty
                                    <td colspan="6" class="text-center">Tidak ada data...</td>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
