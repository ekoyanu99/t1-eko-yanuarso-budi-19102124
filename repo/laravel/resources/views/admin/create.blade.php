@extends('admin_layout.app')
@section('header')
    @include('admin_layout.header')
@endsection
@section('leftbar')
    @include('admin_layout.leftbar')
@endsection
@section('rightbar')
    @include('admin_layout.rightbar')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hoosmartchain
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container pt-4 bg-white">
                <div class="row">
                    <div class="col-md-8 col-xl-6">
                        <h2>Upload Konten</h2>
                        <hr>
                        <form action="{{ route('admin.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="nama">Nama Project</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                                    name="nama" value="{{ old('nama') }}">
                                @error('nama')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                <select class="form-control" name="kategori" id="kategori">
                                    <option value="Project" {{ old('kategori') == 'Project' ? 'selected' : '' }}>
                                        Project
                                    </option>
                                    <option value="Partner" {{ old('kategori') == 'Partner' ? 'selected' : '' }}>
                                        Partner
                                    </option>
                                    <option value="Exchange" {{ old('kategori') == 'Exchange' ? 'selected' : '' }}>
                                        Exchange
                                    </option>
                                    <option value="Wallet" {{ old('kategori') == 'Wallet' ? 'selected' : '' }}>
                                        Wallet
                                    </option>
                                </select>
                                @error('kategori')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" class="form-control @error('website') is-invalid @enderror" id="website"
                                    name="website" value="{{ old('website') }}">
                                @error('website')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="twitter">Twitter</label>
                                <input type="text" class="form-control @error('twitter') is-invalid @enderror" id="twitter"
                                    name="twitter" value="{{ old('twitter') }}">
                                @error('twitter')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="telegram">Telegram</label>
                                <input type="text" class="form-control @error('telegram') is-invalid @enderror"
                                    id="telegram" name="telegram" value="{{ old('telegram') }}">
                                @error('telegram')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" rows="3"
                                    name="deskripsi">{{ old('deskripsi') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="note">Note</label>
                                <textarea class="form-control" id="note" rows="3"
                                    name="note">{{ old('note') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="image">Gambar</label>
                                <input type="file" class="form-control-file" id="image" name="image">
                                @error('image')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mb-2">Post</button>

                        </form>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
