@extends('admin_layout.app')
@section('header')
    @include('admin_layout.header')
@endsection
@section('leftbar')
    @include('admin_layout.leftbar')
@endsection
@section('rightbar')
    @include('admin_layout.rightbar')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Hoosmartchain
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container mt-3">
                <div class="row">
                    <div class="col-12">
                        <div class="pt-3 d-flex justify-content-end align-items-center">
                            <h1 class="h2 mr-auto">Detail {{ $admin->nama }}</h1>
                            <a href="{{ route('admin.edit', ['admin' => $admin->id]) }}" class="btn btn-primary">Edit</a>
                            <form action="{{ route('admin.destroy', ['admin' => $admin->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                            </form>
                        </div>
                        <hr>
                        @if (session()->has('pesan'))
                            <div class="alert alert-success">
                                {{ session()->get('pesan') }}
                            </div>
                        @endif
                        <ul>
                            <li>Nama : {{ $admin->nama }} </li>
                            <li>Kategori : {{ $admin->kategori }} </li>
                            <li>Website : {{ $admin->website }} </li>
                            <li>Twitter : {{ $admin->twitter }} </li>
                            <li>Telegram : {{ $admin->telegram }}</li>
                            <li>Deskripsi : {{ $admin->deskripsi }}</li>
                            <li>Note : {{ $admin->note }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
