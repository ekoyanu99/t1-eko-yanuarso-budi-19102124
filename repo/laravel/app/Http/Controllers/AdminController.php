<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use File;

class AdminController extends Controller
{
    public function create(){
        return view('admin.create');
    }

    public function store(Request $request){
        $validateData = $request->validate([
        'nama' => 'required|min:3|max:50',
        'kategori' => 'required',
        'website' => 'required',
        'twitter' => 'required',
        'telegram' => '',
        'deskripsi' => 'required',
        'note' => 'required',
        'image' => 'required|file|image|max:1000',
        ]);
        $konten = new Admin();
        $konten->nama = $validateData['nama'];
        $konten->kategori = $validateData['kategori'];
        $konten->website = $validateData['website'];
        $konten->twitter = $validateData['twitter'];
        $konten->telegram = $validateData['telegram'];
        $konten->deskripsi = $validateData['deskripsi'];
        $konten->note = $validateData['note'];
        if($request->hasFile('image'))
        {
        $extFile = $request->image->getClientOriginalExtension();
        $namaFile = 'user-'.time().".".$extFile;
        $path = $request->image->move('assets/images',$namaFile);
        $konten->image = $path;
        }

        $konten->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('admin.index');
    }

    public function index(){
        $kontens = Admin::all();
        return view('admin.index',['admins' => $kontens]);
    }

    public function show($admin_id){
        $result = Admin::findOrFail($admin_id);
        return view('admin.show',['admin' => $result]);
    }

    public function edit($admin_id){
        $result = Admin::findOrFail($admin_id);
        return view('admin.edit',['admin' => $result]);
    }

    public function update(Request $request, Admin $admin){
        $validateData = $request->validate([
            'nama' => 'required|min:3|max:50',
            'kategori' => 'required',
            'website' => 'required',
            'twitter' => 'required',
            'telegram' => '',
            'deskripsi' => 'required',
            'note' => 'required',
            'image' => 'file|image|max:1000',
        ]);
        $admin->nama = $validateData['nama'];
        $admin->kategori = $validateData['kategori'];
        $admin->website = $validateData['website'];
        $admin->twitter = $validateData['twitter'];
        $admin->telegram = $validateData['telegram'];
        $admin->deskripsi = $validateData['deskripsi'];
        $admin->note = $validateData['note'];
        if($request->hasFile('image'))
        {
        $extFile = $request->image->getClientOriginalExtension();
        $namaFile = 'user-'.time().".".$extFile;
        File::delete($admin->image);
        $path = $request->image->move('assets/images',$namaFile);
        $admin->image = $path;
        }

        $admin->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('admin.show', ['admin'=>$admin->id]);
    }

    public function destroy(Request $request, Admin $admin)
    {
        File::delete($admin->image);
        $admin->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('admin.index');
    }
}
