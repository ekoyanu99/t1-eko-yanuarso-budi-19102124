<?php

namespace App\Http\Controllers;

use App\AdminLogin;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    public function index()
    {
    return view('login.login');
    }
    public function process(Request $request){
    $validateData = $request->validate([
    'username' => 'required',
    'password' => 'required',
    ]);
    $result = AdminLogin::where('username', '=', $validateData['username'])->first();
    if($result){
    if (($request->password == $result->password)){
    session(['username' => $request->username]);
    return redirect('/admin');
    }
    else {
    return back()->withInput()->with('pesan',"Login Gagal");
    }
    }
    else{
    return back()->withInput()->with('pesan',"Login Gagal");
    }
    }

    public function logout(){
    session()->forget('username');
    return redirect('login')->with('pesan','Logout berhasil');
    }

    public function register(){
        return view('admin.register');
    }

    public function store(Request $request){
        $validateData = $request->validate([
        'username' => 'required',
        'password' => 'required',
        ]);
        $regis = new AdminLogin();
        $regis->username = $validateData['username'];
        $regis->password = $validateData['password'];

        $regis->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('login.register');
    }
}
