<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use File;

class PostController extends Controller
{
    //
    public function index(){
        $posts = Admin::all();
        return view('user.index',['users' => $posts]);
    }

    public function show($admin_id){
        $result = Admin::findOrFail($admin_id);
        return view('user.show',['user' => $result]);
    }
}
